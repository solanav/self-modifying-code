#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <pthread.h>
#include <string.h>

#include "../include/sov.h"

// Should remove this to dynamic size
#define DATA_SIZE 0xffff

/* Private */
int printHex(unsigned char *hex, FILE *output);

int searchMemory(unsigned char **data, int data_size, unsigned char *terms,
		 int terms_size)
{
	int i, j;
	int equal_flag = 0;

	for (i = 0; i <= data_size - terms_size; i++) {
		for (j = 0; j < terms_size; j++) {
			if ((*data)[i + j] == terms[j]) {
				equal_flag = 1;
			} else {
				equal_flag = 0;
				break;
			}
		}

		if (equal_flag == 1) {
			return i;
		} else {
			equal_flag = 0;
		}
	}

	return ERROR;
}

int readFromOffset(void *address, unsigned char ***data, int data_size)
{
	int i;

	void *start_address = NULL;

	if (!address || data_size == 0) {
		printf("[ERROR] Parametters for reading are wrong");
		return ERROR;
	}

	*(data) = (unsigned char **)calloc(DATA_SIZE, sizeof(unsigned char *));
	if (!data) {
		free(data);
		printf("[ERROR] Could not allocate memory\n");
		return ERROR;
	}

	start_address = address;

	if (data_size != UNTIL_C3) {
		for (i = 0; i < data_size; i++) {
			(*data)[i] = (unsigned char *)start_address + i;
		}
	} else {
		i = 0;
		while (*((unsigned char *)start_address + i - 1) != 0xc3) {
			(*data)[i] = (unsigned char *)start_address + i;
			i++;
		}
	}

	return i;
}

int printMemory(unsigned char **data, int data_size, FILE *output)
{
	int i;

	if (!data) {
		printf("[ERROR] No data passed to print\n");
		return ERROR;
	}

	fprintf(output, "%p > ", data[0]);

	for (i = 0; i < data_size; i++) {
		printHex(data[i], output);

		if ((i + 1) % NUMBER_OF_COLUMNS == 0 && i < data_size - 1)
			fprintf(output, "\n%p > ", data[i + 1]);
	}

	fprintf(output, "\n");

	return OK;
}

int printHex(unsigned char *hex, FILE *output)
{
	if (*hex < 0x10) {
		fprintf(output, "0%x ", *hex);
	} else {
		fprintf(output, "%x ", *hex);
	}

	return OK;
}

int changePagePermissions(void *addr)
{
	int page_size = getpagesize();

	if (!addr)
		return ERROR;

	addr -= (unsigned long)addr % page_size;

	if (mprotect(addr, page_size, PROT_READ | PROT_WRITE | PROT_EXEC) == -1)
		return ERROR;

	return OK;
}