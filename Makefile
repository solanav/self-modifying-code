# FLAGS
CC = gcc
CFLAGS = -g -Wall
LDLIBS = -lpthread

# PATHS
SRCPATH = ./src/
HDRPATH = ./include/
OBJPATH = ./build/
BINPATH = ./bin/

# BINARIES
ALL_EXEC = test

# EXEC CREATION
test: test.o sov.o
	$(CC) $(CFLAGS) -o $(BINPATH)test $(OBJPATH)test.o $(OBJPATH)sov.o $(LDLIBS)

# OBJECT CREATION
sov.o: $(SRCPATH)sov.c $(HDRPATH)types.h
	$(CC) $(CFLAGS) -c $(SRCPATH)sov.c -o $(OBJPATH)sov.o 

test.o: $(SRCPATH)test.c $(HDRPATH)sov.h $(HDRPATH)types.h
	$(CC) $(CFLAGS) -c $(SRCPATH)test.c -o $(OBJPATH)test.o 

# COMMANDS

all: clean $(ALL_EXEC)

clean:
	rm -rf $(OBJPATH)* $(BINPATH)*