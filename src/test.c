#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../include/sov.h"

void test()
{
	int i = 0;
	i++;
	printf("%d\n", i);
}

int main()
{
	int search_result_index = 0;
	size_t data_size = 0;

	unsigned char old_mem[1] = { 0x01 };
	unsigned char new_mem[1] = { 0x10 };
	unsigned char **data0 = NULL;
	unsigned char **data1 = NULL;

	changePagePermissions(&test);

	data_size = readFromOffset(&test, &data0, UNTIL_C3);
	printMemory(data0, data_size, stdout);

	search_result_index = searchMemory(data0, data_size, old_mem, 0x1);

	printf("> %p\n", &test + search_result_index);

	if (search_result_index != ERROR) {
		memcpy(&test + search_result_index, new_mem, 0x1);
	}

	data_size = readFromOffset(&test, &data1, UNTIL_C3);
	printMemory(data1, data_size, stdout);

	free(data0);
	free(data1);

	return OK;
}