#ifndef SOV_H
#define SOV_H

#include "../include/types.h"

#define NUMBER_OF_COLUMNS 4
#define UNTIL_C3 -1

int readFromOffset(void *address, unsigned char ***data, int data_size);
int printMemory(unsigned char **data, int data_size, FILE *output);
int changePagePermissions(void *addr);
int searchMemory(unsigned char **data, int data_size, unsigned char *terms,
		 int terms_size);

#endif